Dieses Repository beinhaltet AssemblerQuellcode und ausführbaren Maschinencode für den AtMega8 für meine Ampelsteuerung.

Der Quellcode wurde damals mit dem Atmel AVR studio geschrieben. Der Maschinencode wurde ebenfalls mit dem gleichen Programm erzeugt.
Die Dokumentation wie sie in diesem Repository ist, ist unzureichend um die Ampelsteuerung komplett nachzubauen.

Es fehlt die Dokumentation wie man die Ampelsteuerung zu bauen hat.

Es fehlt die Dokumentation wie der Maschinencode auf den AtMega8 transferiert wird.